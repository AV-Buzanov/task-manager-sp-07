package ru.buzanov.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.rest.AuthResource;
import ru.buzanov.tm.rest.AuthResourceClient;

@Category(IntagratedTests.class)
public class UserRestTest extends Assert {
    @Nullable
    private AuthResource authResource;

    @Before
    public void setUp() throws Exception {
        authResource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        assertNotNull(authResource);
        authResource.register("testUser", "123456");
        authResource.auth("testUser", "123456");
    }

    @After
    public void tearDown() throws Exception {
        authResource.removeUser();
        authResource = null;
    }

    @Test(expected = FeignException.class)
    public void testUserExistLogin() throws FeignException {
        authResource.register("testUser", "123456");
    }

    @Test
    public void testUserMerge() {
        @NotNull final UserDTO user = new UserDTO();
        user.setPasswordHash("321321");
        user.setName("testName");
        user.setLogin("testUser3");
        authResource.mergeProfile(user);
        @NotNull final UserDTO testUser = authResource.user();
        assertEquals(user.getLogin(), testUser.getLogin());
        assertEquals(user.getName(), testUser.getName());
    }
}
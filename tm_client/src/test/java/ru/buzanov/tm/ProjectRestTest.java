package ru.buzanov.tm;

import feign.FeignException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.rest.AuthResource;
import ru.buzanov.tm.rest.AuthResourceClient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Category(IntagratedTests.class)
public class ProjectRestTest extends Assert {
    @Nullable
    private AuthResource authResource;

    @Before
    public void setUp() throws Exception {
        authResource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        assertNotNull(authResource);
        authResource.register("testUser", "123456");
        authResource.auth("testUser", "123456");
    }

    @After
    public void tearDown() throws Exception {
        authResource.removeUser();
        authResource = null;
    }

    @Test
    public void testProjectCreate() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        authResource.mergeProject(project);
        @Nullable final ProjectDTO testProject = authResource.getProject(project.getId());
        assertNotNull(testProject);
        assertEquals("not equals id", project.getId(), testProject.getId());
        assertEquals("not equals userId", authResource.user().getId(), testProject.getUserId());
        assertEquals("not equals Name", project.getName(), testProject.getName());
        assertEquals("not equals createDate", project.getCreateDate().toString(), testProject.getCreateDate().toString());
        assertEquals("not equals status", Status.PLANNED, testProject.getStatus());
    }

    @Test(expected = FeignException.class)
    public void testProjectCreateExistName() throws FeignException {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        authResource.mergeProject(project);
        @NotNull final ProjectDTO testProject = new ProjectDTO();
        testProject.setName("Name");
        authResource.mergeProject(testProject);
    }

    @Test
    public void testProjectMerge() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        project.setDescription("description");
        project.setStartDate(new Date(System.currentTimeMillis()));
        project.setFinishDate(new Date(System.currentTimeMillis()));
        authResource.mergeProject(project);
        project.setName("newName");
        project.setDescription("newDescription");
        project.setStartDate(new Date(System.currentTimeMillis()));
        project.setFinishDate(new Date(System.currentTimeMillis()));
        project.setStatus(Status.IN_PROGRESS);
        authResource.mergeProject(project);
        @Nullable final ProjectDTO testProject = authResource.getProject(project.getId());
        checkEquals(testProject, project);
    }

    @Test
    public void testProjectFind() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        project.setDescription("description");
        project.setStartDate(new Date(System.currentTimeMillis()));
        project.setFinishDate(new Date(System.currentTimeMillis()));
        project.setStatus(Status.DONE);
        authResource.mergeProject(project);
        @Nullable final ProjectDTO testProject = authResource.getProject(project.getId());
        checkEquals(testProject, project);
    }

    @Test
    public void testProjectFindAllSize() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        authResource.mergeProject(project);
        project = new ProjectDTO();
        project.setName("Name2");
        authResource.mergeProject(project);
        project = new ProjectDTO();
        project.setName("Name3");
        authResource.mergeProject(project);
        @Nullable final List<ProjectDTO> list = new ArrayList<>(authResource.listProject());
        assertNotNull(list);
        assertEquals("size error", 3, list.size());
    }

    @Test
    public void testProjectRemove() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        authResource.mergeProject(project);
        authResource.removeProject(project.getId());
        @Nullable final ProjectDTO testProject = authResource.getProject(project.getId());
        assertNull(testProject);
    }

    @Test
    public void testProjectRemoveWithTask() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        authResource.mergeProject(project);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("TaskName");
        task.setProjectId(project.getId());
        authResource.mergeTask(task);
        authResource.removeProject(project.getId());
        @Nullable final ProjectDTO testProject = authResource.getProject(project.getId());
        @Nullable final TaskDTO testTask = authResource.getTask(task.getId());
        assertNull(testProject);
        assertNull(testTask);
    }

    @Test(expected = FeignException.class)
    public void testProjectCreateUnauthorize() throws FeignException {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        AuthResource resource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        resource.mergeProject(project);
    }

    @Test(expected = FeignException.class)
    public void testProjectFindUnauthorize() throws FeignException {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        authResource.mergeProject(project);
        AuthResource resource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        @Nullable final ProjectDTO testProject = resource.getProject(project.getId());
        assertNull(testProject);
    }

    @Test(expected = FeignException.class)
    public void testProjectRemoveUnauthorize() throws FeignException {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        authResource.mergeProject(project);
        AuthResource resource = AuthResourceClient.getInstance("http://localhost:8080/rest");
        resource.removeProject(project.getId());
        @Nullable final ProjectDTO testProject = authResource.getProject(project.getId());
        assertNotNull(testProject);
    }

    private void checkEquals(@Nullable final ProjectDTO testProject, @Nullable final ProjectDTO project) {
        assertNotNull(testProject);
        assertNotNull(project);
        assertEquals("not equals id", project.getId(), testProject.getId());
        assertEquals("not equals userId", authResource.user().getId(), testProject.getUserId());
        assertEquals("not equals Name", project.getName(), testProject.getName());
        assertEquals("not equals desc", project.getDescription(), testProject.getDescription());
        assertEquals("not equals createDate", project.getCreateDate().toString(), testProject.getCreateDate().toString());
        assertEquals("not equals startDate", project.getStartDate().toString(), testProject.getStartDate().toString());
        assertEquals("not equals finishDate", project.getFinishDate().toString(), testProject.getFinishDate().toString());
        assertEquals("not equals status", project.getStatus(), testProject.getStatus());
    }
}
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <title>Project</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/">TASK-MANAGER</a>
  <div>
    <ul class="navbar-nav">
      <li class="nav-item active"><a class="nav-link" href="/projects">Projects</a></li>
      <li class="nav-item"><a class="nav-link" href="/tasks">Tasks</a></li>
      <li class="nav-item"><a class="nav-link" href="/users">Users</a></li>
    </ul>
  </div>
</nav>
<p></p>
<p></p>
<div class="container">
<h2>Project list</h2>
<a class="btn btn-info" href="/projectcreate" role="button">CREATE PROJECT</a>
    <table border="2" style="height: 26px; width: 100%; border-collapse: collapse; border-style: solid; margin-left: auto; margin-right: auto;"><caption>
    <tbody>
        <tr style="height: 18px;">
            <td style="width: 4%; height: 18px;">No</td>
            <td style="width: 27%; height: 18px;">ID</td>
            <td style="width: 16%; height: 18px;">NAME</td>
            <td style="width: 25%; height: 18px;">DESCRIPTION</td>
            <td style="width: 8%; height: 18px;"></td>
            <td style="width: 8%; height: 18px;"></td>
            <td style="width: 8%; height: 18px;"></td>
        </tr>
        <c:forEach var="s" items="${list}">
        <tr style="height: 18px;">
            <td style="width: 4%; height: 18px;">${s.count}</td>
            <td style="width: 27%; height: 18px;">${s.id}</td>
            <td style="width: 16%; height: 18px;">${s.name}</td>
            <td style="width: 25%; height: 18px;">${s.description}</td>
            <td style="width: 8%; height: 18px; text-align:center"><a class="btn btn-light" href="/projectview?id=${s.id}" role="button">view</a></td>
            <td style="width: 8%; height: 18px; text-align:center"><a class="btn btn-warning" href="/projectedit?id=${s.id}" role="button">edit</a></td>
            <td style="width: 8%; height: 18px; text-align:center"><a class="btn btn-danger" href="/projectremove?id=${s.id}" role="button">remove</a></td>
        </tr>
        </c:forEach>
    </tbody>
    </table>
 </div>
</body>
</html>
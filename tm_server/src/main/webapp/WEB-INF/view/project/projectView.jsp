<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <title>Project</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="/">TASK-MANAGER</a>
  <div>
    <ul class="navbar-nav">
      <li class="nav-item active"><a class="nav-link" href="/projects">Projects</a></li>
      <li class="nav-item"><a class="nav-link" href="/tasks">Tasks</a></li>
      <li class="nav-item"><a class="nav-link" href="/users">Users</a></li>
    </ul>
  </div>
</nav>
<p></p>
<p></p>
<div class="container">
<h1>View project</h1>
    <table border="2" style="width: 50%; border-collapse: collapse;">
    <tbody>
        <tr>
            <td style="width: 30%;">id</td>
            <td style="width: 70%;">${project.id}</td>
        </tr>
        <tr>
            <td style="width: 30%;">name</td>
            <td style="width: 70%;">${project.name}</td>
        </tr>
        <tr>
            <td style="width: 30%;">description</td>
            <td style="width: 70%;">${project.description}</td>
        </tr>
        <tr>
            <td style="width: 30%;">create date</td>
            <td style="width: 70%;">${project.createDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">start date</td>
            <td style="width: 70%;">${project.startDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">finish date</td>
            <td style="width: 70%;">${project.finishDate}</td>
        </tr>
        <tr>
            <td style="width: 30%;">status</td>
            <td style="width: 70%;">${project.status}</td>
        </tr>
        <tr>
            <td style="width: 30%;">tasks</td>
            <td style="width: 70%;">
        <c:forEach var="s" items="${tasks}">
            ${s.name}<br>
        </c:forEach>
            </td>
        </tr>
</tbody>
</table>
</div>
<p></p>
<div class="container">
    <a class="btn btn-light" href="/projects" role="button">Back</a>
    </div>

</body>
</html>
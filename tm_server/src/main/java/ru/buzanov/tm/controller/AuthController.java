package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.entity.Roles;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.repository.RoleRepository;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Controller
public class AuthController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PreAuthorize("isAnonymous()")
    @GetMapping("/login")
    public String login(@RequestParam(name = "error", required = false) Boolean error, final Model model) throws Exception {
        if (Boolean.TRUE.equals(error)) {
            model.addAttribute("error", true);
        }
        return "auth/login";
    }

    @GetMapping("/sign-up")
    public String load() throws Exception {
        return "auth/register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute(name = "project") final UserDTO user) throws Exception {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        user.getRoles().add(RoleType.USER);
        userService.load(user);
        return "redirect:/login";
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/profile")
    public String profile(@AuthenticationPrincipal final CustomUser user,
                          final Model model) throws Exception {
        final UserDTO userDTO = userService.findOne(user.getUserId());
        model.addAttribute("user", userDTO);
        final List<String> rolenames = new ArrayList<>();
        for (Roles roles : roleRepository.findAllByUserId(userDTO.getId()))
            rolenames.add(roles.getRoleType().displayName());
        model.addAttribute("roles", rolenames);
        model.addAttribute("projects", projectService.findAll(user.getUserId()));
        return "auth/profile";
    }

    @ExceptionHandler({Exception.class})
    public String handleAll(Exception ex, WebRequest request, Model model) {
        model.addAttribute("msg", ex.getLocalizedMessage());
        return "error";
    }
}

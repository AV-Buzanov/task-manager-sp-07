package ru.buzanov.tm.controller;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.UserService;

@NoArgsConstructor
@RestController
@RequestMapping(value = "/rest")
public class AuthRestController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    public ResponseEntity auth(@RequestHeader("login") String login,
                               @RequestHeader("password") String password) throws Exception {
        try {
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);
            Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return ResponseEntity.ok().build();
        } catch (AuthenticationException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public UserDTO user(@AuthenticationPrincipal CustomUser user) throws Exception {
        return userService.findOne(user.getUserId());
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ResponseEntity logout(@AuthenticationPrincipal CustomUser user) throws Exception {
        SecurityContextHolder.getContext().setAuthentication(null);
        return ResponseEntity.ok().build();
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/profile/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> mergeUser(@AuthenticationPrincipal CustomUser customUser,
                                             @RequestBody final UserDTO user) throws Exception {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        userService.merge(customUser.getUserId(), user);
        return ResponseEntity.ok(userService.findOne(customUser.getUserId()));
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/profile/remove", method = RequestMethod.DELETE)
    public ResponseEntity removeUser(@AuthenticationPrincipal CustomUser customUser) throws Exception {
        SecurityContextHolder.getContext().setAuthentication(null);
        userService.remove(customUser.getUserId());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/register", method = RequestMethod.PUT)
    public ResponseEntity register(@RequestHeader("login") String login,
                                   @RequestHeader("password") String password) throws Exception {
        UserDTO userDTO = new UserDTO();
        userDTO.setPasswordHash(passwordEncoder.encode(password));
        userDTO.setLogin(login);
        userDTO.getRoles().add(RoleType.USER);
        userService.load(userDTO);
        return ResponseEntity.ok().build();
    }
}
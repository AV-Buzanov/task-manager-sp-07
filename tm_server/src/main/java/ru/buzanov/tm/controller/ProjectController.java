package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@PreAuthorize("isAuthenticated()")
public class ProjectController {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;

    @GetMapping("/projects")
    public String projectList(@AuthenticationPrincipal final CustomUser user,
                              final Model model) throws Exception {
        model.addAttribute("list", projectService.findAll(user.getUserId()));
        return "project/project";
    }

    @GetMapping(value = "/projectview")
    public String projectView(@AuthenticationPrincipal final CustomUser user,
                              final Model model,
                              @RequestParam(name = "id") final String id) throws Exception {
        model.addAttribute("project", projectService.findOne(user.getUserId(), id));
        model.addAttribute("tasks", taskService.findByProjectId(user.getUserId(), id));
        return "project/projectView";
    }

    @GetMapping(value = "/projectedit")
    public String projectEdit(@AuthenticationPrincipal final CustomUser user,
                              final Model model,
                              @RequestParam(name = "id") final String id) throws Exception {
        final Map<Status, String> statuses = new LinkedHashMap<>();
        for (Status s : Status.values())
            statuses.put(s, s.displayName());
        model.addAttribute("project", projectService.findOne(user.getUserId(), id));
        model.addAttribute("statuses", statuses);
        return "project/projectEdit";
    }

    @PostMapping(value = "/merge")
    public String projectMerge(@AuthenticationPrincipal final CustomUser user,
                               @ModelAttribute(name = "project") final ProjectDTO project) throws Exception {
        projectService.merge(user.getUserId(), project.getId(), project);
        return "redirect:/projects";
    }

    @GetMapping(value = "/projectremove")
    public String projectRemove(@AuthenticationPrincipal final CustomUser user,
                                @RequestParam(name = "id") final String id) throws Exception {
        projectService.remove(user.getUserId(), id);
        return "redirect:/projects";
    }

    @GetMapping(value = "/projectcreate")
    public String projectCreate(@AuthenticationPrincipal final CustomUser user,
                                final Model model) throws Exception {
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(user.getUserId());
        model.addAttribute("project", project);
        final Map<Status, String> statuses = new LinkedHashMap<>();
        for (Status s : Status.values())
            statuses.put(s, s.displayName());
        model.addAttribute("statuses", statuses);
        return "project/projectEdit";
    }

    @ExceptionHandler({Exception.class})
    public String handleAll(Exception ex, WebRequest request, Model model) {
        model.addAttribute("msg", ex.getLocalizedMessage());
        return "error";
    }
}

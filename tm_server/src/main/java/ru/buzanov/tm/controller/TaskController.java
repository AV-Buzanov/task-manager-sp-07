package ru.buzanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.security.CustomUser;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@PreAuthorize("isAuthenticated()")
public class TaskController {
    @Autowired
    private TaskService taskService;
    @Autowired
    private ProjectService projectService;

    @GetMapping("/tasks")
    public String taskList(@AuthenticationPrincipal final CustomUser user,
                           final Model model) throws Exception {
        model.addAttribute("list", taskService.findAll(user.getUserId()));
        return "task/task";
    }

    @GetMapping(value = "/taskview")
    public String taskView(@AuthenticationPrincipal final CustomUser user,
                           final Model model,
                           @RequestParam(name = "id") final String id) throws Exception {
        final TaskDTO task = taskService.findOne(user.getUserId(), id);
        model.addAttribute("task", task);
        model.addAttribute("project", projectService.findOne(user.getUserId(), task.getProjectId()));
        return "task/taskView";
    }

    @GetMapping(value = "/taskedit")
    public String taskEdit(@AuthenticationPrincipal final CustomUser user,
                           final Model model,
                           @RequestParam(name = "id") final String id) throws Exception {
        final Map<Status, String> statuses = new LinkedHashMap<>();
        for (Status s : Status.values())
            statuses.put(s, s.displayName());
        model.addAttribute("task", taskService.findOne(user.getUserId(), id));
        model.addAttribute("statuses", statuses);
        model.addAttribute("projects", projectService.findAll(user.getUserId()));
        return "task/taskEdit";
    }

    @PostMapping(value = "/taskmerge")
    public String taskMerge(@AuthenticationPrincipal final CustomUser user,
                            @ModelAttribute(name = "task") final TaskDTO task) throws Exception {
        taskService.merge(user.getUserId(), task.getId(), task);
        return "redirect:/tasks";
    }

    @GetMapping(value = "/taskremove")
    public String taskRemove(@AuthenticationPrincipal final CustomUser user,
                             @RequestParam(name = "id") final String id) throws Exception {
        taskService.remove(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping(value = "/taskcreate")
    public String taskCreate(@AuthenticationPrincipal final CustomUser user,
                             final Model model) throws Exception {
        final TaskDTO task = new TaskDTO();
        task.setUserId(user.getUserId());
        final Map<Status, String> statuses = new LinkedHashMap<>();
        for (Status s : Status.values())
            statuses.put(s, s.displayName());
        model.addAttribute("task", task);
        model.addAttribute("statuses", statuses);
        model.addAttribute("projects", projectService.findAll(user.getUserId()));
        return "task/taskEdit";
    }

    @ExceptionHandler({Exception.class})
    public String handleAll(Exception ex, WebRequest request, Model model) {
        model.addAttribute("msg", ex.getLocalizedMessage());
        return "error";
    }
}

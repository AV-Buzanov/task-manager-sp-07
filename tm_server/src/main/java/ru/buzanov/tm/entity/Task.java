package ru.buzanov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="app_task")
public class Task extends AbstractWBS {
    @Nullable
    @ManyToOne
    @JoinColumn(name="project_id")
    private Project project;
}
package ru.buzanov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.buzanov.tm.configuration.AppConfig;
import ru.buzanov.tm.dto.ProjectDTO;
import ru.buzanov.tm.dto.TaskDTO;
import ru.buzanov.tm.dto.UserDTO;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;
import ru.buzanov.tm.service.UserService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Category(DataTests.class)
@DirtiesContext
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class ProjectServiceTest extends Assert {
    @Nullable
    @Resource
    private ProjectService projectService;
    @Nullable
    @Resource
    private TaskService taskService;
    @Nullable
    @Resource
    private UserService userService;
    private UserDTO user;
    private UserDTO user2;

    @Before
    public void setUp() throws Exception {
        user = new UserDTO();
        user.setLogin("login");
        user.setPasswordHash("123456");
        user.getRoles().add(RoleType.USER);
        userService.load(user);
        user2 = new UserDTO();
        user2.setLogin("login2");
        user2.setPasswordHash("123456");
        user2.getRoles().add(RoleType.USER);
        userService.load(user2);
    }

    @After
    public void tearDown() throws Exception {
        userService.remove(user.getId());
        userService.remove(user2.getId());
    }

    @Test
    public void testProjectCreate() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectService.load(user.getId(), project);
        @Nullable final ProjectDTO testProject = projectService.findOne(user.getId(), project.getId());
        assertNotNull(testProject);
        assertEquals("not equals id", project.getId(), testProject.getId());
        assertEquals("not equals userId", user.getId(), testProject.getUserId());
        assertEquals("not equals Name", project.getName(), testProject.getName());
        assertEquals("not equals createDate", project.getCreateDate().getTime(), testProject.getCreateDate().getTime());
        assertEquals("not equals status", Status.PLANNED, testProject.getStatus());
    }

    @Test(expected = Exception.class)
    public void testProjectCreateExistName() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectService.load(user.getId(), project);
        @NotNull final ProjectDTO testProject = new ProjectDTO();
        testProject.setName("Name");
        projectService.load(user.getId(), testProject);
    }

    @Test
    public void testProjectMerge() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        project.setDescription("description");
        project.setStartDate((new Date(System.currentTimeMillis())));
        project.setFinishDate((new Date(System.currentTimeMillis())));
        projectService.load(user.getId(), project);
        project.setName("newName");
        project.setDescription("newDescription");
        project.setStartDate((new Date(System.currentTimeMillis())));
        project.setFinishDate((new Date(System.currentTimeMillis())));
        project.setStatus(Status.IN_PROGRESS);
        projectService.merge(user.getId(), project.getId(), project);
        @Nullable final ProjectDTO testProject = projectService.findOne(user.getId(), project.getId());
        checkEquals(testProject, project);
    }

    @Test
    public void testProjectFind() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        project.setDescription("description");
        project.setStartDate((new Date(System.currentTimeMillis())));
        project.setFinishDate((new Date(System.currentTimeMillis())));
        project.setStatus(Status.PLANNED);
        projectService.load(user.getId(), project);
        @Nullable final ProjectDTO testProject = projectService.findOne(user.getId(), project.getId());
        checkEquals(testProject, project);
    }

    @Test
    public void testProjectFindAllSize() throws Exception {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectService.load(user.getId(), project);
        project = new ProjectDTO();
        project.setName("Name2");
        projectService.load(user.getId(), project);
        project = new ProjectDTO();
        project.setName("Name3");
        projectService.load(user.getId(), project);
        @Nullable final List<ProjectDTO> list = new ArrayList<>(projectService.findAll(user.getId()));
        assertNotNull(list);
        assertEquals("size error", 3, list.size());
    }

    @Test
    public void testProjectRemove() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectService.load(user.getId(), project);
        projectService.remove(user.getId(), project.getId());
        @Nullable final ProjectDTO testProject = projectService.findOne(user.getId(), project.getId());
        assertNull(testProject);
    }

    @Test
    public void testProjectRemoveWithTask() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectService.load(user.getId(), project);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName("TaskName");
        task.setProjectId(project.getId());
        projectService.remove(user.getId(), project.getId());
        @Nullable final ProjectDTO testProject = projectService.findOne(user.getId(), project.getId());
        @Nullable final TaskDTO testTask = taskService.findOne(user.getId(), task.getId());
        assertNull(testProject);
        assertNull(testTask);
    }

    @Test
    public void testProjectFindWithWrongUser() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectService.load(user.getId(), project);
        @Nullable final ProjectDTO testProject = projectService.findOne(user2.getId(), project.getId());
        assertNull(testProject);
    }

    @Test
    public void testProjectRemoveWithWrongUser() throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Name");
        projectService.load(user.getId(), project);
        projectService.remove(user2.getId(), project.getId());
        assertNotNull(projectService.findOne(user.getId(), project.getId()));
    }

    private void checkEquals(@Nullable final ProjectDTO testProject, @Nullable final ProjectDTO project) {
        assertNotNull(testProject);
        assertNotNull(testProject.getStartDate());
        assertNotNull(testProject.getFinishDate());
        assertNotNull(project);
        assertNotNull(project.getStartDate());
        assertNotNull(project.getFinishDate());
        assertEquals("not equals id", project.getId(), testProject.getId());
        assertEquals("not equals userId", user.getId(), testProject.getUserId());
        assertEquals("not equals Name", project.getName(), testProject.getName());
        assertEquals("not equals desc", project.getDescription(), testProject.getDescription());
        assertEquals("not equals createDate", project.getCreateDate().getTime(), testProject.getCreateDate().getTime());
        assertEquals("not equals startDate", project.getStartDate().getTime(), testProject.getStartDate().getTime());
        assertEquals("not equals finishDate", project.getFinishDate().getTime(), testProject.getFinishDate().getTime());
        assertEquals("not equals status", project.getStatus(), testProject.getStatus());
    }
}